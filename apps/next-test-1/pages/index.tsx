export function Index(props) {
  return <div>test{props.test}</div>;
}

export const getServerSideProps = async () => {
  return {
    props: {
      test: 'hello!',
    },
  };
};

export default Index;
